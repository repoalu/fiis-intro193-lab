import conductor
import reclamos
import demo_tkinter as demo
def mostrar_inicio():
    print()
    print("*************************")
    print("Bienvenido Administrador")
    print("*************************")
    print("Seleccione una opcion:")
    print("1. Registro de conductor")
    print("2. Atencion de reclamos")
    print("3. Reportes")
    print("4. Demo tkinter")
    opcion = input("Seleccione una opcion: ")
    if(opcion == "1"):
        conductor.registrar_conductor()
    elif(opcion == "2"):
        reclamos.mostrar_reclamos()
    elif(opcion == "4"):
        demo.mostrar_ventana()
