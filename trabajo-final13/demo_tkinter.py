from tkinter import *

def mostrar_ventana():
    window = Tk() 
    window.title("Bienvenidos a Taxi FIIS")
    window.geometry('500x350')
    lbl = Label(window, text="Nombre")
    lbl.grid(column=0, row=0)
    txt = Entry(window,width=10)
    txt.grid(column=1, row=0)
    def clicked(): 
        lbl.configure(text="Boton presionado")
    btn = Button(window, text="Presione", command=clicked)
    btn.grid(column=2, row=0)
    window.mainloop()
    