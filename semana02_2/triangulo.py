a = float(input("Ingrese lado 1:"))
b = float(input("Ingrese lado 2:"))
c = float(input("Ingrese lado 3:"))

per = a + b + c
print("Perimetro: ", per)

p = per / 2
area = (p * (p - a) * (p - b) * (p - c)) ** 0.5
print("Area: ", area)
print("Equilatero: ", (a == b) and (a == c))
print( "Rectangulo:",
       (a ** 2 == b ** 2 + c ** 2) or
       (b ** 2 == a ** 2 + c ** 2) or
       (c ** 2 == a ** 2 + b ** 2)
      )

