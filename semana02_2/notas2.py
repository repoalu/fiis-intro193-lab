def mostrar_resultado(nota, nota_min):
    if(nota < nota_min):
        print("Alumno desaprobado")
    else:
        print("Alumno aprobado")

nota = int(input("Ingrese nota del alumno: "))
nota_min = int(input("Ingrese nota minima aprobatoria:"))
mostrar_resultado(nota, nota_min)