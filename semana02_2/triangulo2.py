def mostrar_tipo(a, b, c):
    if((a == b) and (b == c)):
        print("Equilatero")
    elif((a == b) or (a == c) or (b == c)):
        print("Isosceles")
    else:
        print("Escaleno")

    maximo = max(a, b, c)
    sc_menores = (a ** 2 + b ** 2 + c ** 2) - maximo  ** 2
    if(maximo ** 2 < sc_menores):
        print("Acutangulo")
    elif(maximo ** 2 == sc_menores):
        print("Rectangulo")
    else:
        print("Obtusangulo")
        
    
#Pruebas
mostrar_tipo(3, 4, 5)
mostrar_tipo(5, 5, 5)
mostrar_tipo(5, 5, 6)
mostrar_tipo(3, 4, 6)
