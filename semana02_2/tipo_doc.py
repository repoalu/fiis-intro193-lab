def mostrar_descripcion(tipo_doc):
    if(tipo_doc == 1):
        print("RUC")
    elif(tipo_doc == 2):
        print("DNI")
    elif(tipo_doc == 3):
        print("Carnet de Extranjeria")

    else:
        print("Tipo Doc incorrecto")

#Probar
mostrar_descripcion(1)
mostrar_descripcion(3)
mostrar_descripcion(-8)
mostrar_descripcion(500)