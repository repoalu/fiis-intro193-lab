def convertir_dolares(monto_dolares, tipo_cambio):
    monto_soles = monto_dolares * tipo_cambio
    return monto_soles

#Pruebas
prod1 = 200
prod2 = 150
tc = 3.35

prod1_soles = convertir_dolares(prod1, tc)
prod2_soles = convertir_dolares(prod2, tc)
suma = prod1_soles + prod2_soles
print("Monto total en soles:", suma)