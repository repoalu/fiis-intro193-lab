def mostrar_descripcion(edad):
    if(0 <= edad < 18):
        print("Menor de edad")
    elif(18 <= edad < 65):
        print("Adulto")
    elif(edad >= 65):
        print("Adulto mayor")
    else:
        print("Valor incorrecto")
        
mostrar_descripcion(18)
mostrar_descripcion(12)
mostrar_descripcion(64)
mostrar_descripcion(66)
mostrar_descripcion(-2)
        