def leer_datos():
    t = int(input())
    for i in range(t):
        linea1 = input()
        valores = convertir(linea1)
        n = valores[0]
        d = valores[1]
        linea2 = input()
        pilas = convertir(linea2)
        print(n, d)
        print(pilas)

'''
Funcion de conversion
entrada:  "1 0 3 2"
salida: [1, 0, 3, 2] 
'''
def convertir(texto):
    #TODO: Implementar
    respuesta = []
    palabra = ""
    for i in range(len(texto)):
        if(texto[i] == " "):
            respuesta.append(int(palabra))
            palabra = ""
        else:
            palabra += texto[i] 
    respuesta.append(int(palabra))
    return respuesta


leer_datos()
