def es_palindromo(cadena):
    i = 0
    j = len(cadena) - 1
    palindromo = True
    while(i <= j):
        if(cadena[i] != cadena[j]):
            palindromo = False
            break
        i += 1
        j -= i
    return palindromo

def main():
    print(es_palindromo("ABC"))
    print(es_palindromo("ABBA"))
    print(es_palindromo("ANA"))

#Pruebas
#main()
            