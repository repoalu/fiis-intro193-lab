import p01
def mostrar_disponibles(asientos):
    disponibles = 0
    for i in range(len(asientos)):
        for j in range(len(asientos[0])):
            if(asientos[i][j] == "0"):
                letra = chr(ord("A") + i)
                print("Disponible:", letra + str(j + 1))
    return disponibles


def main():

    asientos = ["XX0", "00X", "0X0", "XX0"]
    p01.imprimir_matriz(asientos)
    disp = mostrar_disponibles(asientos)
    print("Asientos disponibles:", disp)

#Pruebas
main()