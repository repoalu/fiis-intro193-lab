import p01
def calcular_disponibles(asientos):
    disponibles = 0
    for i in range(len(asientos)):
        for j in range(len(asientos[0])):
            if(asientos[i][j] == "0"):
                disponibles += 1
    return disponibles


def main():

    asientos = ["XX0", "XXX", "0X0", "XX0"]
    p01.imprimir_matriz(asientos)
    disp = calcular_disponibles(asientos)
    print("Asientos disponibles:", disp)

#Pruebas
main()