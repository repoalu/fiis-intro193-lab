def mostrar_estadisticas(ventas):
    #Producto mas vendido
    prod_max = 0
    vtas_max = 0
    for i in range(len(ventas)):
        suma = 0
        for j in range(len(ventas[0])):
            suma += ventas[i][j]
        if(suma > vtas_max):
            vtas_max = suma
            prod_max = i + 1
    print(f"El producto {prod_max} es el mas vendido")
    print("Ventas:", vtas_max)
    
    trim_max = 0
    vtas_max = 0 
    for j in range(len(ventas[0])):
        suma = 0
        for i in range(len(ventas)):
            suma += ventas[i][j]
        if(suma > vtas_max):
            vtas_max = suma
            trim_max = j + 1
    print(f"El trimestre {trim_max} tuvo la mayor cantidad de ventas")
    print("Ventas:", vtas_max)
            

            
def main():
    ventas = [[100, 400, 500, 850],
              [280, 620, 770, 920],
              [800, 850, 880, 930],
              [600, 550, 400, 900],
              [100, 180, 600, 850]]
    mostrar_estadisticas(ventas)
    
#Pruebas
main()