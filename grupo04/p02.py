import p01
def es_simetrica(M):
    respuesta = True
    for i in range(len(M)):
        for j in range(len(M[0])):
            if(M[i][j] != M[j][i]):
                respuesta = False
    return respuesta

def evaluar_simetrica(M):
    res = es_simetrica(M)
    if(res == True):
        print("La matriz es simetrica")
    else:
        print("la matriz no es simetrica")

def main():
    matriz1 = [ [1, 0, 1],
                [0, 0, 1],
                [1, 1, 1]]

    matriz2 = [ [1, 2, 3],
                [2, 5, 6],
                [7, 8, 9]]
    
    p01.imprimir_matriz(matriz1)
    evaluar_simetrica(matriz1)
    
    p01.imprimir_matriz(matriz2)
    evaluar_simetrica(matriz2)
    
#Pruebas
main()