def obtener_caracter(texto):
    respuesta = ""
    for i in range(len(texto)):
        if(texto[i] not in texto[i + 1:]):
            respuesta = texto[i]
            break
    return respuesta

def buscar(texto, letra):
    encontrado = False
    for i in range(len(texto)):
        if(texto[i] == letra):
            encontrado = True
            break
    return encontrado

print(obtener_caracter("abcdcba"))

            