'''
Para el problema anterior, 
implemente un programa que permita, dado un valor "k", 
retornar Verdadero en caso existan "k" asientos disponibles 
consecutivos y Falso en caso contrario.
'''
import p01
def tiene_consecutivos(asientos, k):
    res = False
    for i in range(len(asientos)):
        consecutivos = 0    
        for j in range(len(asientos[0])):
            if(asientos[i][j] == "0"):
                consecutivos += 1
            elif(asientos[i][j] == "X"):
                if(consecutivos >= k or j == len(asientos[0]) - 1):
                    res = True
                consecutivos = 0
        if(consecutivos >= k):
            res = True
    return res


def main():

    asientos = ["XX0", "0XX", "0X0", "X00"]
    p01.imprimir_matriz(asientos)
    respuesta = tiene_consecutivos(asientos, 2)
    print("Tiene consecutivos:", respuesta)

#Pruebas
main()
