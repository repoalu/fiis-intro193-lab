import aleatorios

def devolver_unicos(lista):
    unicos = []
    for i in range(len(lista)):
        encontrado = False
        for j in range(len(unicos)):
            if(lista[i] == unicos[j]):
                encontrado = True
        if(encontrado == False):
            unicos.append(lista[i])
    return unicos

def main():
    n = 15
    lista = aleatorios.generar_aleatorios(n, 1, 10)
    print(lista)
    unicos = devolver_unicos(lista)
    print(unicos)
main()
