def calcular_moda(L):
    moda = 0
    maximo = 0
    for i in range(len(L)):
        contador = 1
        for j in range(i + 1, len(L)):
            if(L[i] == L[j]):
                contador += 1
        if(contador > maximo):
            moda = L[i]
            maximo = contador
    return moda

def main():
    lista = [5, 8, 2, 4, 10, 7, 8, 3, 9, 4, 7, 5, 9, 4, 4]
    moda = calcular_moda(lista)
    print("La moda es", moda)
    
main()