def evaluar_polinomio(lista, x):
    res = 0
    for i in range(len(lista)):
        exponente = len(lista) - 1 - i
        coeficiente = lista[i]
        res += coeficiente * (x) ** exponente
    return res

def evaluar_expresion(P, Q, x):
    px = evaluar_polinomio(P, x)
    qx = evaluar_polinomio(Q, x)
    return px + qx

def main():
    P = [1, 2, 3, 5]
    Q = [1, 1, 1]
    x = 3
    resultado = evaluar_expresion(P, Q, x)
    print("P(x) + Q(x) =", resultado)
    
#Pruebas
main()