def sumar_matrices(M, N):
    suma = []
    for i in range(len(M)):
        fila = []
        for j in range(len(M[i])):
            elemento = M[i][j] + N[i][j]
            fila.append(elemento)
        suma.append(fila)
    return suma

def main():
    M = [[1, 1, 1], [1, 0, 1], [2, 1, 2]]
    N = [[1, 1, 1], [1, 0, 1], [2, 1, 2]]
    suma = sumar_matrices(M, N)
    print(suma)
#Pruebas
main()

