import random
def generar_aleatorios(N, inf, sup):
    lista = []
    for i in range(N):
        numero = random.randint(inf, sup)
        lista.append(numero)
    return lista

def calcular_promedio(lista):
    suma = 0
    for i in range(len(lista)):
        suma += lista[i]
    return suma / len(lista)

def imprimir_comparacion(lista, valor_ref):
    mayores = 0
    menores = 0
    for i in range(len(lista)):
        if(lista[i] > valor_ref):
            mayores += 1
        elif(lista[i] < valor_ref):
            menores += 1
    print(f"Existen {mayores} valores mayores que valor_ref")        
    print(f"Existen {menores} valores menores que valor_ref")        
    
def main():
    n = 100
    valores = generar_aleatorios(n, 100, 200)
    prom = calcular_promedio(valores)
    print(valores)
    print("El promedio es: ", prom)
    imprimir_comparacion(valores, prom)

#Pruebas
#main()