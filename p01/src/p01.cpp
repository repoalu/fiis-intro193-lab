//============================================================================
// Name        : p01.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

void def_variables(){
	int a = 5;
	cout << a << endl;
	float g = 4.5;
	double h = 4.23444;
	char c = 'C';
	int b = 10;
	int z = a + b;
	z = z + 1; //z++
	string s = "ABC";
	cout << "Valor de g: " << g << endl;
	cout << "Valores de h y c: " << h << " " << c << endl;

}

void tipodatos(){
	float a = 3;
	float b = 2;
	float c = 3.2;
	float perimetro = a + b + c;
	int perimetro2 = a + b + c;
	cout << perimetro << " " << perimetro2;
}

void tipodatos2(){
	int base = 3;
	int altura = 5;
	float area = 1.0 * base * altura / 2;
	cout << "El area es: " << area;
}

void selectiva(){
	int edad = 22;
	if(edad < 18){
		cout << "Menor de edad";
	}else if(edad < 65){
		cout << "Adulto";
	}else{
		cout << "Adulto mayor";
	}
}

void repetitivas(){
	int n = 100;
	int suma = 0;
	for(int i = 1; i <= 100; i++){
		suma += i;
	}
	cout << suma;
}

void arrays(){
	int a[2];
	a[0] = 12;
	a[1] = 2;

	a[0] = 15;
	cout << a[0];

	int b[] = {8, 12, 16};
	cout << b[1];
}

void recorrido_arrays(){
	int numeros[] = {8, 12, 16, 20};
	//
	int cantElem = 4;
	for(int i = 0; i < cantElem; i++){
		cout << numeros[i] << endl;
	}
	//
}


int main() {
	int v1[] = {8, 11, 123, 33};
	int v2[] = {2, 22, 441, 23};
	int longitud = 4;
	for(int i = 0; i < longitud; i++){
		int elemento = v1[i] * v2[i];
		cout << elemento << endl;
	}
	return 0;
}




















