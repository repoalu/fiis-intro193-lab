lista = [10, 12.5, 15, 15.3]
'''
range(4) -> range(0, 4) -> 0 1 2 3
'''
for i in range(len(lista)):
    print(f"Elemento {i}: {lista[i]}")
