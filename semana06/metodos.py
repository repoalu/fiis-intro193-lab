nombre = "pedro"
print(nombre.capitalize())
print(nombre)

texto1 = "ABC"
print(texto1.isalpha())

dni = "45585658"
print(dni.isdigit())

texto = "Los alumnos juegan en los campos"
print(texto.find("los"))
print(texto.count("los"))
print(texto.upper())
print(texto.upper().count("LOS"))

