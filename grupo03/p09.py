def obtener_lista_parciales(lista, cant_elem):
    respuesta = []
    suma = 0
    for i in range(cant_elem):
        suma = suma + lista[i]
        respuesta.append(suma)
    return respuesta

def main():
    lista = [12, 32, 3, 34, 55]
    lista2 = obtener_lista_parciales(lista, len(lista))
    print(lista2)

#Pruebas
main()