def barajar(L1, L2):
    cant1 = len(L1)
    cant2 = len(L2)
    contador = 0
    respuesta = []
    mayor = cant1
    if(cant2 > cant1):
        mayor = cant2

    while(contador < mayor):
        if(contador < cant1):
            respuesta.append(L1[contador])
        if(contador < cant2):
            respuesta.append(L2[contador])
        contador = contador + 1
    return respuesta

def main():
    L1 = [11, 13, 15, 17]
    L2 = [12, 14, 16, 18, 20, 22]
    L3 = barajar(L1, L2)
    print(L3)

#Pruebas
main()

