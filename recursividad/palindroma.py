def palindroma_rec(palabra):
    if(len(palabra) <= 1):
        return True
    elif(palabra[0] != palabra[len(palabra) - 1]):
        return False
    else:
        return palindroma_rec(palabra[1:len(palabra) - 1])

def main():
    str1 = "ABC"
    str2 = "AdNdA"
    print(palindroma_rec(str1))
    print(palindroma_rec(str2))
    
#Pruebas
main()