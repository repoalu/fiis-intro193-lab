def obtener_mayor(lista):
    actual = lista[0]
    if(len(lista) == 1):
        return actual
    else:
        mayor = obtener_mayor(lista[1:])
        if(mayor < actual):
            return actual
        else:
            return mayor
def main():
    lista = [12, 11, 24, 5, 124, 55]
    print(obtener_mayor(lista))

#Pruebas
main()