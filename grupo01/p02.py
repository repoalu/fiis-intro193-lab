import funciones as f

def calcular_promedio(n1, n2, n3, n4):
    suma = n1 + n2 + n3 + n4
    menor = f.calcular_minimo(n1, n2, n3, n4)
    promedio = (suma - menor) / 3
    return promedio

#Pruebas
prom = calcular_promedio(15, 15, 14, 5)
print("Promedio:", prom)