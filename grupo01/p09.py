'''
Obtiene la cantidad de meses de diferencia entre la fecha actual y la fecha de nacimiento del bebe
Se representa la fecha de nacimiento con 3 numeros enteros: dia, mes y anio respectivamente (d1, m1, a1)
De manera similar con la fecha actual: d2, m2, a2.
El algoritmo asume que fecha2 > fecha 1 y que se cumple que las fechas se diferencia como maximo en 3 meses
'''
def obtener_meses_bebe(d1, m1, a1, d2, m2, a2):
    if(a1 == a2):
        meses = m2 - m1        
    elif(a2 == a1 + 1):
        meses = 12 - m1 + m2
    
    #Mes todavia no se cumple
    if(d1 > d2):
        meses = meses - 1
    
    return meses

def leer_controles():
    #Datos fecha actual
    d1 = 20
    m1 = 4
    a1 = 2019
    
    #Datos fecha usuario
    d2 = int(input("Ingrese dia (nro) de nacimiento del bebe: "))
    m2 = int(input("Ingrese mes (nro) de nacimiento del bebe: "))
    a2 = int(input("Ingrese anio de nacimiento del bebe: "))
    
    meses = obtener_meses_bebe(d1, m1, a1, d2, m2, a2)
    nro_control = 1
    anterior = 0
    actual = 0

    if(meses - nro_control >= 0):
        anterior = actual
        actual = float(input(f"Ingrese peso de control en mes {nro_control}: "))
        if(actual - anterior < 0.3 and nro_control > 1):
            print(f"En el mes {nro_control} hay una diferencia menor que 300 gramos respecto del control anterior")
        nro_control = nro_control + 1
        
    if(meses - nro_control >= 0):
        anterior = actual
        actual = float(input(f"Ingrese peso de control en mes {nro_control}: "))
        if(actual - anterior < 0.3 and nro_control > 1):
            print(f"En el mes {nro_control} hay una diferencia menor que 300 gramos respecto del control anterior")
        nro_control = nro_control + 1    

    if(meses - nro_control >= 0):
        anterior = actual
        actual = float(input(f"Ingrese peso de control en mes {nro_control}: "))
        if(actual - anterior < 0.3 and nro_control > 1):
            print(f"En el mes {nro_control} hay una diferencia menor que 300 gramos respecto del control anterior")
        nro_control = nro_control + 1    
#Pruebas
leer_controles()