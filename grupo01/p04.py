import funciones as f

def obtener_total(subtotal):
    total = subtotal + f.calcular_igv(subtotal)
    return total

#Pruebas
subt = float(input("Ingrese subtotal: "))
total = obtener_total(subt)
print("Total a pagar:", total)