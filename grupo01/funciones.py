def calcular_minimo(n1, n2, n3, n4):
    minimo = n1
    if(n2 < minimo):
        minimo = n2
    if(n3 < minimo):
        minimo = n3
    if(n4 < minimo):
        minimo = n4
    return minimo

def calcular_igv(precio):
    return 0.18 * precio