def f(x):
    y = x ** 2 + 4 * x + 5
    return y
    
print(f(0))
print(f(1))
print(f(2))
print(f(3))
print(f(400))
