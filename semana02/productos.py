tasa_igv = 0.18
p1 = float(input("Ingrese precio p1:"))
q1 = int(input("Ingrese cantidad p1:"))

p2 = float(input("Ingrese precio p2:"))
q2 = int(input("Ingrese cantidad p2:"))

subtotal = p1 * q1 + p2 * q2
igv = tasa_igv * subtotal
total = subtotal + igv

print("Subtotal:", subtotal)
print("IGV:", igv)
print("Total:", total)
