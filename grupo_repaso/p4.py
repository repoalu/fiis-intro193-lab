def cifrar_cesar(texto, n):
    respuesta = ""
    for i in range(len(texto)):                
        letra = texto[i]
        if(letra != " "):
            codigo = ord(letra) + n
            nueva_letra = chr(codigo)
        else:
            nueva_letra = letra
        respuesta += nueva_letra    
    return respuesta

def obtener_texto_original(texto, primera_palabra):
    respuesta = ""
    n = ord(texto[0]) - ord(primera_palabra[0])
    for i in range(len(texto)):                
        letra = texto[i]
        if(letra != " "):
            codigo = ord(letra) - n
            nueva_letra = chr(codigo)
        else:
            nueva_letra = letra
        respuesta += nueva_letra
    return respuesta      
    
def main():
    texto = "hola mundo"
    n = 2
    cifrado = cifrar_cesar(texto, n)
    print("Texto original:", texto)
    print("Texto cifrado:", cifrado)
    
    original = obtener_texto_original(cifrado, "hola")
    print("Decodificado:", original)
    
#Pruebas
main()