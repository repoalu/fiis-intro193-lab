def es_primo(numero):
    contador = 0
    for i in range(1, numero + 1):
        if(numero % i == 0):
            contador += 1
    if(contador == 2):
        return True
    else:
        return False

def es_mariana(lista, cant_elementos):
    respuesta = True 
    central = cant_elementos // 2
    if(es_primo(lista[central]) == False):
        print("Elemento central no es primo")
        respuesta = False
    else:
        suma = lista[0] + lista[cant_elementos - 1]
        i = 0
        j = cant_elementos - 1
        no_cumplen = 0
        while(i < j):
            suma2 = lista[i] + lista[j]
            if(suma2 != suma):
                no_cumplen += 1
            i += 1
            j -= 1
        if(no_cumplen != 0):
            print(f"{no_cumplen} par(es) de valores no cumplen condicion")
            respuesta = False
    return respuesta

def main():
    lista1 = [10, 2, 18, 29, 7, 1, 12, 28, 20]
    lista2 = [44, 26, 36, 45, 11, 57, 63, 74, 56]
    print(es_mariana(lista1, len(lista1)))
    print(es_mariana(lista2, len(lista2)))

#Pruebas
main()

