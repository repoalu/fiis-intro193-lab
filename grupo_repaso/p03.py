def mostrar_reporte(lista_mediciones):
    MIN_AUMENTO = 200
    for i in range(len(lista_mediciones)):
        tiene_problemas = False
        for j in range(1, len(lista_mediciones[0])):
            if(lista_mediciones[i][j] != -1 and lista_mediciones[i][j] - lista_mediciones[i][j - 1] < MIN_AUMENTO):
                tiene_problemas = True
        if(tiene_problemas == True):
            print(f"Especimen {i + 1} con problemas de desarrollo")
        
def main():
    mediciones =    [   [200, 500, 800, 1600, 1700],
                        [400, 700, 900, -1, -1],
                        [200, -1, -1, -1, -1]                            
                    ]
    mostrar_reporte(mediciones)

#Pruebas
main()
