#...truncable por la izquierda
from math import log
def es_primo(n):
    suma = 0
    primo = False
    for i in range(1, n + 1):
        if(n % i == 0):
            suma = suma + 1
    if(suma == 2):
        primo = True
    return primo

def quitar_digito_izq(n):
    cant_digitos = int(log(n, 10)) + 1
    return n % (10 ** (cant_digitos - 1))

def es_truncable_izq(n):
    truncable = True
    while(n > 0 and truncable == True):
        if(es_primo(n) == False):
            truncable = False
        n = quitar_digito_izq(n)
    return truncable

print(es_truncable_izq(123123))
print(es_truncable_izq(2617))