#Con esto solucionamos p08
def convertir_decimal(n, b):
    factor = 1
    respuesta = 0
    while(n > 0):
        digito = n % b
        n = n // b
        respuesta = respuesta + digito * factor
        factor = factor * 10
    return respuesta

def mostrar_secuencia(k, b):
    for i in range(1, k + 1):
        nro = convertir_decimal(i, b)
        print(nro, end = " ")
    print()
def main():
    mostrar_secuencia(10, 3)
    mostrar_secuencia(18, 7)

#Pruebas
main()
    