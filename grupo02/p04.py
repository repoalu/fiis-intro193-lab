def obtener_digitos(N):
    menor = 9
    mayor = 0
    while(N > 0):
        digito = N % 10
        N = N // 10
        if(digito < menor):
            menor = digito
        if(digito > mayor):
            mayor = digito
    print("Mayor:", mayor)
    print("Menor:", menor)    
    
def main():
    obtener_digitos(23425845)

#Pruebas
main()
