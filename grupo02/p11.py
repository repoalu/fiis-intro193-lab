from math import log
a = log(1000, 10)
print(int(a))

#Obtiene el i-esimo digito contando desde la izquierda
def obtener_digito(N, i):
    return (N // 10 ** (i - 1)) % 10

def es_capicua(N):
    #Otra opcion: utilizar la funcion de calculo de digitos del material de clase
    cant_digitos = int(log(N, 10)) + 1
    i1 = cant_digitos
    i2 = 1
    capicua = True
    while(i2 < i1 and capicua == True):
        dig1 = obtener_digito(N, i1)
        dig2 = obtener_digito(N, i2)
        if(dig1 != dig2):
            capicua = False
        i1 -= 1 
        i2 += 1 
    return capicua

def main():
    print(es_capicua(415524))
    print(es_capicua(334))
    print(es_capicua(3443))
    
#Pruebas
main()