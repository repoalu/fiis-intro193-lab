def calcular_numero(N):
    factor = 1
    resultado = 0
    while(N >= 100):
        dig1 = N % 10
        dig2 = (N // 10) % 10
        dig3 = (N // 100) % 10
        if(dig1 > dig2 and dig1 > dig3):
            resultado += (factor * dig1)
            factor = factor * 10
        N = N // 10
    return resultado

def main():
    res = calcular_numero(126729)
    print(res)

#Pruebas
main()