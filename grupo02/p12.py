def calcular_expresion(k):
    t1 = 1
    t2 = 1
    t3 = 2
    t4 = 4
    suma = t1 + t2 + t3 + t4
    print("Serie:", t1, t2, t3, t4, end = " ")
    for i in range(k - 4):
        t1 = t2
        t2 = t3
        t3 = t4
        t4 = t1 + t2 + t3
        suma += t4
        print(t4, end = " ")
    print()
    return suma

def main():
    print("Suma:", calcular_expresion(5))
    print("Suma:", calcular_expresion(7))
#Pruebas
main()