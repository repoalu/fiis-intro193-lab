#import iostream as io
def selectiva():
    edad = 22
    if(edad < 18):
        print("Menor de edad")
    elif(edad < 65):
        print("Adulto")
    else:
        print("Adulto mayor")

def repetitivas():
    n = 100
    suma = 0
    for i in range(1, n + 1):
        suma += i
    print(suma)

def listas():
    a = [12, 2]
    a[0] = 15
    print(a[0])
    
    b = [8, 12, 16]
    print(b[1])
    
def recorrido_listas():
    numeros = [8, 12, 16, 20]
    print(numeros)
    for i in range(len(numeros)):
        print(numeros[i])
    
    print("*****************")
    for elemento in numeros:
        print(elemento)    

def prod_listas():
    v1 = [8, 11, 123, 33]
    v2 = [2, 22, 441, 23]
    for i in range(len(v1)):
        elemento = v1[i] * v2[i]
        print(elemento)


def main():
    prod_listas()

#Pruebas
main()